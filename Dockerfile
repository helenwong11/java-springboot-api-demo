FROM openjdk:11
COPY build/libs/java-springboot-api-demo-0.0.2.jar java-springboot-api-demo.jar
ENTRYPOINT ["java","-jar","/java-springboot-api-demo.jar"]

#FROM openjdk:11
#FROM alpine:3.13.5

# RUN apt-get update \
#       && apt-get install -y sudo \
#       && rm -rf /var/lib/apt/lists/*

#ARG JAR_FILE=build/libs/*.jar
#COPY ${JAR_FILE} app.jar
#ENTRYPOINT ["java","-jar","/app.jar"]
#EXPOSE 8085