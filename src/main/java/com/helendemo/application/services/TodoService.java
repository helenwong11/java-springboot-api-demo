package com.helendemo.application.services;

import com.helendemo.application.models.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TodoService {
    private List<Todo> todos = new ArrayList<Todo>();
    private int idCounter = 0;

    TodoService(){
        this.todos = new ArrayList<Todo>();
        this.todos.add(new Todo("Testing Todo"));
    }

    public List<Todo> getTodos(){
        return todos;
    }

    public int addTodo(Todo todo){
        idCounter++;
        todo.setId(idCounter);
        this.todos.add(todo);
        return idCounter;

    }
}
