package com.helendemo.application.controller;

import com.helendemo.application.forms.TodoForm;
import com.helendemo.application.models.Todo;
import com.helendemo.application.services.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TodoController {

    @Autowired
    private TodoService todoService;

    @RequestMapping(value="/todos", method= RequestMethod.GET)
    public List<Todo> getTodos(){
        return this.todoService.getTodos();
    }

    @RequestMapping(value="/todos", method= RequestMethod.POST)
    public int createTodo(@RequestBody TodoForm todoForm){
        int id = todoService.addTodo(todoForm.toTodo());
        return id;


    }
}
