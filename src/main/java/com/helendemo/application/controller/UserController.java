package com.helendemo.application.controller;


import com.helendemo.application.aspect.GeneralInterceptorAspect;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/user")
@Api(value = "User Resource REST Endpoint", description = "Shows the user info")
public class UserController {

   // private Logger log = LoggerFactory.getLogger(GeneralInterceptorAspect.class);

    @GetMapping
    public List<User> getUsers() {

//        log.info("request", request);
//        System.out.println("request: " + request);
        return Arrays.asList(
                new User("Sam G", 5000L),
                new User("Peter A", 5000L)
        );
    }

    @GetMapping("/{userName}")
    public User getUser(@PathVariable("userName") final String userName) {
        return new User(userName, 6000L);
    }

    private class User {

        @ApiModelProperty(notes = "name of the User", required = true)
        private String userName;

        @ApiModelProperty(notes = "salary of the user")
        private Long salary;

        //@ApiModelProperty(notes = "id of the user")
        //private String userId;
        

        public User(String userName, Long salary) {
            this.userName = userName;
            this.salary = salary;
            //this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        //public String getUserId() {
        //    return userId;
        //}

        //public void setUserId(String userID) {
        //    this.userId = userId;
        //}

        public Long getSalary() {
            return salary;
        }

        public void setSalary(Long salary) {
            this.salary = salary;
        }
    }
}