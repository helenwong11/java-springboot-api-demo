package com.helendemo.application.aspect;

import com.helendemo.application.models.LogModel;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Enumeration;


@Aspect
@Configuration
public class GeneralInterceptorAspect extends HttpServlet {

    /*
     * Logging
     * Exception Handling
     * Time Taken
     */

    private Logger log = LoggerFactory.getLogger(GeneralInterceptorAspect.class);

    @Pointcut(value = "execution(* com.helendemo.application.controller.*.*(..))")
    public void controller() {
    }

    @Pointcut("execution(* *.*(..))")
    protected void allMethod() {
    }

    @Pointcut("execution(* *.*(..))")
    protected void aroundMethod() {
    }

    // Log Model Object
    @Around("controller() && allMethod() ")
    public Object aroundMethod(ProceedingJoinPoint pjp) throws Throwable {
        long start = System.currentTimeMillis();
        try {
            String className = pjp.getSignature().getDeclaringTypeName();
            String methodName = pjp.getSignature().getName();
            System.out.println("-------------This will run BEFORE-----------");

            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

            LogModel logmodel = new LogModel();

            if (null != request) {
                logmodel.setMethodType(request.getMethod());

                Enumeration headerNames = request.getHeaderNames();
                logmodel.setHeaderNames(headerNames);

            }
            logmodel.setServletPath(request.getServletPath());
            logmodel.setRequestURI(request.getRequestURI());
            logmodel.setClassName(className);
            logmodel.setMethodName(methodName);

            Object result = pjp.proceed();
            long elapsedTime = System.currentTimeMillis() - start;

            logmodel.setElapsedTime(elapsedTime);
            logmodel.setHost(request.getHeader("host"));
            logmodel.setConnection(request.getHeader("connection"));
            logmodel.setContentLength(request.getHeader("content-length"));
            logmodel.setSecChUa(request.getHeader("sec-ch-ua"));
            logmodel.setAccept(request.getHeader("accept"));
            logmodel.setContentType(request.getHeader("content-type"));
            logmodel.setSecChUaMobile(request.getHeader("sec-ch-ua-mobile"));
            logmodel.setUserAgent(request.getHeader("user-agent"));
            logmodel.setSecChUaPlatform(request.getHeader("sec-ch-ua-platform"));
            logmodel.setOrigin(request.getHeader("origin"));
            logmodel.setSecFetchSite(request.getHeader("sec-fetch-site"));
            logmodel.setSecFetchMode(request.getHeader("sec-fetch-mode"));
            logmodel.setSecFetchDest(request.getHeader("sec-fetch-dest"));
            logmodel.setReferer(request.getHeader("referer"));
            logmodel.setAcceptEncoding(request.getHeader("accept-encoding"));
            logmodel.setAcceptLanguage(request.getHeader("accept-language"));
            logmodel.setCookie(request.getHeader("cookie"));
            logmodel.setApiPath(logmodel.getReferer() + request.getRequestURI());


            System.out.println(logmodel.getApiPath());
            System.out.println(logmodel.getElapsedTime());
            System.out.println(logmodel.getHost());
            System.out.println(logmodel.getConnection());
            System.out.println(logmodel.getContentLength());
            System.out.println(logmodel.getSecChUa());
            System.out.println(logmodel.getAccept());
            System.out.println(logmodel.getContentType());
            System.out.println(logmodel.getSecChUaMobile());
            System.out.println(logmodel.getUserAgent());
            System.out.println(logmodel.getSecChUaPlatform());
            System.out.println(logmodel.getOrigin());
            System.out.println(logmodel.getSecFetchSite());
            System.out.println(logmodel.getSecFetchMode());
            System.out.println(logmodel.getSecFetchDest());
            System.out.println(logmodel.getReferer());
            System.out.println(logmodel.getAcceptEncoding());
            System.out.println(logmodel.getAcceptLanguage());
            System.out.println(logmodel.getCookie());
            System.out.println(logmodel.getMethodType());
            System.out.println(logmodel.getServletPath());
            System.out.println(logmodel.getRequestURI());
            System.out.println(logmodel.getClassName());
            System.out.println(logmodel.getMethodName());
            System.out.println(logmodel.getHeaderNames());
            System.out.println("-------------This will run AFTER-----------");
            return result;

        } catch (IllegalArgumentException e) {
            log.error("Illegal argument " + Arrays.toString(pjp.getArgs()) + " in "
                    + pjp.getSignature().getName() + "()");
            throw e;
        }

    }

    ;

}
