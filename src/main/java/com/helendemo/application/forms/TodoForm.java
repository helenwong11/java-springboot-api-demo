package com.helendemo.application.forms;

import com.helendemo.application.models.Todo;


// Form Object
public class TodoForm {

    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Todo toTodo(){
        return new Todo(this.content);
    }
}
